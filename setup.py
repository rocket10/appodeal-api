from setuptools import setup, find_packages

setup(
    name='appodeal-api',
    version='0.1.1',
    author='Aslan',
    author_email='bloogrox@gmail.com',
    packages=find_packages(),
    url='https://bitbucket.org/rocket10/appodeal-api',
    description='Python client for Appodeal API',
    install_requires=[
        "requests",
    ]
)
